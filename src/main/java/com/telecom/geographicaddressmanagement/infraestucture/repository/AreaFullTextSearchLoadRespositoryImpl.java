/**
 * 
 */
package com.telecom.geographicaddressmanagement.infraestucture.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.telecom.geographicaddressmanagement.domain.model.DataArea;
import com.telecom.geographicaddressmanagement.domain.repository.AreaFullTextSearchLoadRespository;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@Repository
public class AreaFullTextSearchLoadRespositoryImpl implements AreaFullTextSearchLoadRespository {
	
	/* (non-Javadoc)
	 * @see com.telecom.geographicaddressmanagement.domain.repository.AreaFullTextSearchLoadRespository#byPage(java.lang.Long, java.lang.Long)
	 */

	@Override
	public List<DataArea> byPage(final int page, final int size) {
		DataArea pais = null;
		DataArea provincia = null;
		DataArea partido = null;
		DataArea localidad = null;
		DataArea barrio = null;
		List<DataArea> result = new ArrayList<DataArea>();
		

		/* PAIS*/
		pais = new DataArea();
		pais.setId(1L);
		pais.setFathers(null);
		pais.setName("ARGENTINA");
		pais.setCharacteristics("Coordenada X de punto medio#-63.0875304387117;Coordenada Y de punto medio#-32.28623324361");
		pais.setIdentification("ARGENTINA");
		pais.setType("Paises");		
		result.add(pais);
		
		/*PROVINCIAS */
		provincia = new DataArea();
		provincia.setId(20230L);
		provincia.setFathers(";1#Paises#ARGENTINA");
		provincia.setName("BUENOS AIRES");
		provincia.setCharacteristics("Coordenada X de punto medio#-60.080246842956;Coordenada Y de punto medio#-37.1646736066089");
		provincia.setIdentification("ARGENTINA,BUENOS AIRES");
		provincia.setType("Provincias");		
		result.add(provincia);
		
		provincia = new DataArea();
		provincia.setId(30611L);
		provincia.setFathers(";1#Paises#ARGENTINA");
		provincia.setName("CAPITAL FEDERAL");
		provincia.setCharacteristics("Coordenada X de punto medio#-58.43304961964;Coordenada Y de punto medio#-34.618661266688");
		provincia.setIdentification("ARGENTINA,CAPITAL FEDERAL");
		provincia.setType("Provincias");		
		result.add(provincia);
		
		/*PARTIDOS*/
		partido = new DataArea();
		partido.setId(521L);
		partido.setFathers(";1#Provincias#BUENOS AIRES;2#Paises#ARGENTINA");
		partido.setName("SAN MIGUEL");
		partido.setCharacteristics("Coordenada X de punto medio#-58.6909542204404;Coordenada Y de punto medio#-34.5534816056391");
		partido.setIdentification("ARGENTINA,BUENOS AIRES,SAN MIGUEL");
		partido.setType("Partidos");		
		result.add(partido);
		
		partido = new DataArea();
		partido.setId(1001L);
		partido.setFathers(";1#Provincias#BUENOS AIRES;2#Paises#ARGENTINA");
		partido.setName("TRES DE FEBRERO");
		partido.setCharacteristics("Coordenada X de punto medio#-58.5840176016147;Coordenada Y de punto medio#-34.6004026620745");
		partido.setIdentification("ARGENTINA,BUENOS AIRES,TRES DE FEBRERO");
		partido.setType("Partidos");		
		result.add(partido);
		
		partido = new DataArea();
		partido.setId(1001L);
		partido.setFathers(";1#Provincias#CAPITAL FEDERAL;2#Paises#ARGENTINA");
		partido.setName("CAPITAL FEDERAL");
		partido.setCharacteristics("Coordenada X de punto medio#-58.4330362519042;Coordenada Y de punto medio#-34.6193723515205");
		partido.setIdentification("ARGENTINA,CAPITAL FEDERA,CAPITAL FEDERAL");
		partido.setType("Partidos");		
		result.add(partido);
		
		
		/*LOCALIDADES*/
		localidad = new DataArea();
		localidad.setId(47704724L);
		localidad.setFathers(";1#Partidos#SAN MIGUEL;2#Provincias#BUENOS AIRES;3#Paises#ARGENTINA");
		localidad.setName("BELLA VISTA");
		localidad.setCharacteristics("Coordenada X de punto medio#-58.695353614986;Coordenada Y de punto medio#-34.5741286696393");
		localidad.setIdentification("ARGENTINA,BUENOS AIRES,SAN MIGUEL,BELLA VISTA");
		localidad.setType("Localidades");		
		result.add(localidad);
		
		localidad = new DataArea();
		localidad.setId(47704899L);
		localidad.setFathers(";1#Partidos#SAN MIGUEL;2#Provincias#BUENOS AIRES;3#Paises#ARGENTINA");
		localidad.setName("CAMPO DE MAYO");
		localidad.setCharacteristics("Coordenada X de punto medio#-58.6538767779563;Coordenada Y de punto medio#-34.5337775462729");
		localidad.setIdentification("ARGENTINA,BUENOS AIRES,SAN MIGUEL,CAMPO DE MAYO");
		localidad.setType("Localidades");		
		result.add(localidad);
		
		localidad = new DataArea();
		localidad.setId(47704899L);
		localidad.setFathers(";1#Partidos#SAN MIGUEL;2#Provincias#BUENOS AIRES;3#Paises#ARGENTINA");
		localidad.setName("SAN MIGUEL");
		localidad.setCharacteristics("Coordenada X de punto medio#-58.7186061154896;Coordenada Y de punto medio#-34.536817396909");
		localidad.setIdentification("ARGENTINA,BUENOS AIRES,SAN MIGUEL,SAN MIGUEL");
		localidad.setType("Localidades");		
		result.add(localidad);

		localidad = new DataArea();
		localidad.setId(1217L);
		localidad.setFathers(";1#Partidos#TRES DE FEBRERO;2#Provincias#BUENOS AIRES;3#Paises#ARGENTINA");
		localidad.setName("CASEROS");
		localidad.setIdentification("ARGENTINA,BUENOS AIRES,TRES DE FEBRERO,CASEROS");
		localidad.setCharacteristics("Coordenada X de punto medio#-58.5635929182823;Coordenada Y de punto medio#-34.609745016875");
		localidad.setType("Localidades");		
		result.add(localidad);
		
		localidad = new DataArea();
		localidad.setId(1229L);
		localidad.setFathers(";1#Partidos#TRES DE FEBRERO;2#Provincias#BUENOS AIRES;3#Paises#ARGENTINA");
		localidad.setName("VILLA BOSCH");
		localidad.setIdentification("ARGENTINA,BUENOS AIRES,TRES DE FEBRERO,VILLA BOSCH");
		localidad.setCharacteristics("Coordenada X de punto medio#-58.5797774220502;Coordenada Y de punto medio#-34.5817818063894");
		localidad.setType("Localidades");		
		result.add(localidad);
		
		localidad = new DataArea();
		localidad.setId(1221L);
		localidad.setFathers(";1#Partidos#TRES DE FEBRERO;2#Provincias#BUENOS AIRES;3#Paises#ARGENTINA");
		localidad.setName("CHURRUCA");
		localidad.setIdentification("ARGENTINA,BUENOS AIRES,TRES DE FEBRERO,CHURRUCA");
		localidad.setCharacteristics("Coordenada X de punto medio#-58.6191677806475;Coordenada Y de punto medio#-34.5542399413003");
		localidad.setType("Localidades");		
		result.add(localidad);
	
		localidad = new DataArea();
		localidad.setId(47L);
		localidad.setFathers(";1#Partidos#CAPITAL FEDERAL;2#Provincias#CAPITAL FEDERAL;3#Paises#ARGENTINA");
		localidad.setName("CAPITAL FEDERAL");
		localidad.setIdentification("ARGENTINA,CAPITAL FEDERAL,CAPITAL FEDERAL,CAPITAL FEDERAL");
		localidad.setCharacteristics("Coordenada X de punto medio#-58.4331095863921;Coordenada Y de punto medio#-34.6154710809204");
		localidad.setType("Localidades");		
		result.add(localidad);
		
		
		/*BARRIOS*/
		barrio = new DataArea();
		barrio.setId(135L);
		barrio.setFathers(";1#Localidades#CAPITAL FEDERAL;2#Partidos#CAPITAL FEDERAL;3#Provincias#CAPITAL FEDERAL;4#Paises#ARGENTINA");
		barrio.setName("AGRONOMIA");
		barrio.setIdentification("ARGENTINA,CAPITAL FEDERAL,CAPITAL FEDERAL,CAPITAL FEDERAL,AGRONOMIA");
		barrio.setCharacteristics("Coordenada X de punto medio#-58.4898567929958;Coordenada Y de punto medio#-34.592381115095");
		barrio.setType("Barrios");		
		result.add(barrio);
		
		barrio = new DataArea();
		barrio.setId(128L);
		barrio.setFathers(";1#Localidades#CAPITAL FEDERAL;2#Partidos#CAPITAL FEDERAL;3#Provincias#CAPITAL FEDERAL;4#Paises#ARGENTINA");
		barrio.setName("PALERMO");
		barrio.setIdentification("ARGENTINA,CAPITAL FEDERAL,CAPITAL FEDERAL,CAPITAL FEDERAL,PALERMO");
		barrio.setCharacteristics("Coordenada X de punto medio#-58.4227131304588;Coordenada Y de punto medio#-34.5741546750923");
		barrio.setType("Barrios");		
		result.add(barrio);
		
		barrio = new DataArea();
		barrio.setId(139L);
		barrio.setFathers(";1#Localidades#CAPITAL FEDERAL;2#Partidos#CAPITAL FEDERAL;3#Provincias#CAPITAL FEDERAL;4#Paises#ARGENTINA");
		barrio.setName("CHACARITA");
		barrio.setIdentification("ARGENTINA,CAPITAL FEDERAL,CAPITAL FEDERAL,CAPITAL FEDERAL,CHACARITA");
		barrio.setCharacteristics("Coordenada X de punto medio#-58.4558188760875;Coordenada Y de punto medio#-34.58798451655");
		barrio.setType("Barrios");		
		result.add(barrio);
		
		return result;
		
	}

}
