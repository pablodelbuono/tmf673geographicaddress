/**
 * 
 */
package com.telecom.geographicaddressmanagement.infraestucture.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.telecom.geographicaddressmanagement.domain.model.DataAddress;
import com.telecom.geographicaddressmanagement.domain.model.RelationArea;
import com.telecom.geographicaddressmanagement.domain.repository.GeographicAddressRepository;
import com.telecom.geographicaddressmanagement.resource.v1.request.RequestGeographicAddressSearchByAddress;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@Repository
public class GeographicAddressRepositoryImpl implements GeographicAddressRepository {
	

	@Override
	public DataAddress getDataAddressBy(RequestGeographicAddressSearchByAddress address) {
	
		DataAddress direccion = null;
		RelationArea area = null;
		List<RelationArea> areas = null;
		
		area = new RelationArea();
		area.setType("Zona Competencia");
		area.setValue("TELECENTRO");
		areas = new ArrayList<>();
		areas.add(area);
	
		direccion = new DataAddress();
		direccion.setCountry(address.getCountry().toUpperCase());
		direccion.setStateOrProvince(address.getStateOrProvince().toUpperCase());
		direccion.setCity(address.getCity().toUpperCase());
		direccion.setLocality(address.getLocality().toUpperCase());
		direccion.setStreetName(address.getStreetName().toUpperCase());
		direccion.setStreetNr(address.getStreetNr());
		direccion.setIntersectionLeft("CALLE IZQUIEDA");
		direccion.setIntersectionRight("CALLE DERECHA");		
		direccion.setPostcode("1414");
		direccion.setStreetNumberSide("IMPAR");
		direccion.setStreetType("AVENIDA");
		direccion.setZones(areas);	
		
		direccion.setSpatialRef("WSG84");
		direccion.setGeometryType("POINT");
		direccion.setX("-58.4227131304588");
		direccion.setY("-34.5741546750923");
			
		
		return direccion;
	}

}
