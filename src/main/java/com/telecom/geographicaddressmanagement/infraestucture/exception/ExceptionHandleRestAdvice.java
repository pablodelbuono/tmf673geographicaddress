/**
 * 
 */
package com.telecom.geographicaddressmanagement.infraestucture.exception;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@RestControllerAdvice
public class ExceptionHandleRestAdvice {
	private static final Logger logger = LoggerFactory.getLogger(ExceptionHandleRestAdvice.class);
	
	private static final String ERROR_MSG_GENERIC = "Error tecnico. Por favor contacte el administrador";
	private static final String ERROR_CODE_GENERIC = "INTERNAL_SERVER_ERROR";
	
	@Value("${resource.cache.area:60}")
	private int HTTP_CACHE_MAX_AGE; 

	
	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<CustomErrorResponse> handleGenericException(Exception ex) {		
		CustomErrorResponse customErrorResponse = null;
		ResponseEntity<CustomErrorResponse> response = null;
		
		customErrorResponse = new CustomErrorResponse(ERROR_CODE_GENERIC, ERROR_MSG_GENERIC);
		customErrorResponse.setTimestamp(LocalDateTime.now());
		customErrorResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
				
		logger.error("Error de ejecucion: {}", ex);
		response = new ResponseEntity<>(customErrorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		

		return response;
	}
	
	@ExceptionHandler(value = NotFoundException.class)
	public ResponseEntity<CustomErrorResponse> handleNotFoundException(NotFoundException ex) {		
		CustomErrorResponse customErrorResponse = null;
		ResponseEntity<CustomErrorResponse> response = null;
		
		customErrorResponse = new CustomErrorResponse(HttpStatus.NOT_FOUND.name(), ex.getMessage());
		customErrorResponse.setTimestamp(LocalDateTime.now());
		customErrorResponse.setStatus(HttpStatus.NOT_FOUND.value());
				
		response = ResponseEntity.status(HttpStatus.NOT_FOUND).cacheControl(CacheControl.maxAge(HTTP_CACHE_MAX_AGE, TimeUnit.SECONDS)).body(customErrorResponse);
		logger.info("Respuesta: {}", response);

		return response;
	}

}
