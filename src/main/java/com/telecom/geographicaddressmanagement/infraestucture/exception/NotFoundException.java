/**
 * 
 */
package com.telecom.geographicaddressmanagement.infraestucture.exception;

/**
 * @author PABLODANIELDELBUONO
 *
 */
public class NotFoundException extends BusinessException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String MSG = "NOT FOUND RESOURCE";
	
	public NotFoundException() {
		super(MSG);
	}

}
