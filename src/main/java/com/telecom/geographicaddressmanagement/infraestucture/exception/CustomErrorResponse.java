/**
 * 
 */
package com.telecom.geographicaddressmanagement.infraestucture.exception;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * @author PABLODANIELDELBUONO
 *
 */
//@ApiModel(description = "Model to create a new Vehicle")
@Data
public class CustomErrorResponse {
	String errorCode;
	String errorMessage;
//	   @ApiModelProperty(notes = "HTTP Status Code")
	Integer status;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
	LocalDateTime timestamp;
	
	public CustomErrorResponse(final String errorCode, final String errorMessage ) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
}
