/**
 * 
 */
package com.telecom.geographicaddressmanagement.config;

import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.spring.starter.embedded.InfinispanCacheConfigurer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@Configuration
@EnableCaching
public class CacheConfig {
	
	@Bean
	public InfinispanCacheConfigurer cacheConfigurer() {
	  return cacheManager -> {
	     final org.infinispan.configuration.cache.Configuration config =
	           new ConfigurationBuilder()
	           		.clustering().cacheMode(CacheMode.LOCAL)
//	           		.memory().size(CACHE_SIZE)
//	           		.expiration().wakeUpInterval(CACHE_LIFE_SPAN, TimeUnit.HOURS).maxIdle(CACHE_MAX_IDLE, TimeUnit.HOURS)
//	           		.memory().evictionType(EvictionType.COUNT)
	                .jmxStatistics().enable()
	                .build();

	     cacheManager.defineConfiguration("areafulltextsuggest", config);
	     cacheManager.defineConfiguration("areafulltextareatype", config);
	     cacheManager.defineConfiguration("areafulltextfather", config);
	     cacheManager.defineConfiguration("areafulltextarea", config);	     
	     cacheManager.defineConfiguration("areabytypename", config);
	  };
	}


}
