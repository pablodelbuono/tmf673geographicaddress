/**
 * 
 */
package com.telecom.geographicaddressmanagement.resource.v1.request;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@Data
public class RequestAreaByType {
	
	@ApiParam(value = "area type", required = true, example = "Localidades")
	@NotBlank
	@Size(min=1, max=50)
	private String type;
	
	@ApiParam(value = "Requested number of resources to be provided in response requested by client. Default value 999", defaultValue = "999", example = "999") 
	@Min(0)
	@Max(999)
	private Integer limit = 999;
	
	@ApiParam(value = "Requested index for start of resources to be provided in response requested by clien. Default value 0",  defaultValue = "0", example = "0") 
	@Min(0)
	@Max(999)
	private Integer offset = 0;
	
	@ApiParam(value = "area father type", required = false, example = "Paises") 
	@Size(min=0, max=50)
	private String fatherType;
	
	@ApiParam(value = "area father unique identification", required = false, allowMultiple = true, example = "CAPITAL FEDERAL")
	@Size(max=20)
	private List<@NotBlank @Size(min=1, max=50) String> fatherIdentification;

}
