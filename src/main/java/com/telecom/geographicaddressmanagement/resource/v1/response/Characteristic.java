/**
 * 
 */
package com.telecom.geographicaddressmanagement.resource.v1.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@Data
@ApiModel( description="Name/value pairs, used to extra characterized the Area")
public class Characteristic {
	@ApiModelProperty(value = "Name of the characteristic.", required = true, example = "Codigo Postal")
	private String name;
	@ApiModelProperty(value = "Value of the characteristic.", required = true, example = "1234")
	private String value;
}
