/**
 * 
 */
package com.telecom.geographicaddressmanagement.resource.v1;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telecom.geographicaddressmanagement.config.SwaggerConfig;
import com.telecom.geographicaddressmanagement.resource.v1.request.RequestStreetByAreaFather;
import com.telecom.geographicaddressmanagement.resource.v1.request.RequestStreetByIdentification;
import com.telecom.geographicaddressmanagement.resource.v1.request.RequestStreetFullTextSearch;
import com.telecom.geographicaddressmanagement.resource.v1.response.Street;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@RestController
@RequestMapping(value = "v1/streets", produces = MediaType.APPLICATION_JSON_UTF8_VALUE) // , consumes =
																						// MediaType.APPLICATION_JSON_VALUE)
@Api(tags = { SwaggerConfig.TAG_STREET })
public class StreetResource {

//	@GetMapping(params= {"name"})
//	@ApiOperation(value = "Get street by name")
//	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved data from street."),
//			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found. Not found streets.") })	
//	public ResponseEntity<List<Street>> searchByName(final @NotBlank @Size(min=1, max=50)  String name) {
//		return null;
//	}	
	
	@GetMapping(params= {"name", "country", "stateOrProvince", "city", "locality"})
	@ApiOperation(value = "Get street by identification")
	@ApiImplicitParam(name = "fields", paramType = "query",  value = "property filters, selects properties of an object using a subset of the Facebook Graph API filtering syntax", required = false, dataTypeClass = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved data from street."),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found. Not found streets.") })	
	public ResponseEntity<Street> searchByIdentification(@Valid RequestStreetByIdentification identification) {
		return null;
	} 
	
	@GetMapping(params= {"name", "fullText=true"})
	@ApiOperation(value = "Full text search by street name")
	@ApiImplicitParam(name = "fields", paramType = "query",  value = "property filters, selects properties of an object using a subset of the Facebook Graph API filtering syntax", required = false, dataTypeClass = String.class)
	@ApiParam(value = "full text search", required = true, example = "true")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved data from street."),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found. Not found streets.") })
	public ResponseEntity<List<Street>> searchBySuggest(@Valid RequestStreetFullTextSearch streetFullTextSearch) {
		return null;
	}
	
	@GetMapping(params= {"name", "fullText=false"})
	@ApiOperation(value = "Get street by name")
	@ApiParam(value = "full text search", required = true, example = "false") 
	@ApiImplicitParam(name = "fields", paramType = "query",  value = "property filters, selects properties of an object using a subset of the Facebook Graph API filtering syntax", required = false, dataTypeClass = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved data from street."),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found. Not found streets.") })
	public ResponseEntity<List<Street>> searchByName(@Valid RequestStreetFullTextSearch streetFullTextSearch) {
		return null;
	}
	
	
	@GetMapping(params= {"areaFatherName"})
	@ApiOperation(value = "Get street by area fhater")
	@ApiImplicitParam(name = "fields", paramType = "query",  value = "property filters, selects properties of an object using a subset of the Facebook Graph API filtering syntax", required = false, dataTypeClass = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved data from street."),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found. Not found streets.") })
	public ResponseEntity<List<Street>> searchByAreaFather(@Valid RequestStreetByAreaFather areaFather) {
		return null;
	}
	
}
