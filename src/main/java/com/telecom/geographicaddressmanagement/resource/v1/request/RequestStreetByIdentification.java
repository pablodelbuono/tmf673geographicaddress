/**
 * 
 */
package com.telecom.geographicaddressmanagement.resource.v1.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@Data
public class RequestStreetByIdentification {
	@ApiModelProperty(value = "Country that the address is in.", required = true, example = "ARGENTINA")
	private String country;
	
	@ApiModelProperty(value = "The State or Province that the address is in.", required = true, example = "CAPITAL FEDERAL")
	private String stateOrProvince;
	
	@ApiModelProperty(value = "City that the address is in.", required = true, example = "CAPITAL FEDERAL")
	private String city;
	
	@ApiModelProperty(value = "Locality that the address is in.", required = true, example = "CAPITAL FEDERAL")
	private String locality;
	
	@ApiParam(value = "street name", required = true, example = "SERRANO")
	@NotBlank
	@Size(min=1, max=50)
	private String name;
}
