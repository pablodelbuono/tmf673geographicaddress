/**
 * 
 */
package com.telecom.geographicaddressmanagement.resource.v1.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telecom.geographicaddressmanagement.config.SwaggerConfig;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@Data
@ApiModel( description=SwaggerConfig.TAG_AREA_DESCRIPTION)
public class Area {
	@JsonIgnore
	private Long id;
	
	@ApiModelProperty(value = "Area name.", required = true, example = "ARGENTINA")
	private String name;
	
	@ApiModelProperty(value = "Identification", required = true, example = "ARGENTINA,BUENOS AIRES,TRES FEBRERO,CHURRUCA")
	private String identification;
	
	@ApiModelProperty(value = "Areas type", required = true, example = "Paises")
	private String type;
	
	@ApiModelProperty(value = "List of characteristics")
	private List<Characteristic> characteristics = new ArrayList<>();
	
	@ApiModelProperty(value = "List of area father")
	private List<AreaFather> fathers = new ArrayList<>(); 
}
