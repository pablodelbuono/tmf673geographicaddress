/**
 * 
 */
package com.telecom.geographicaddressmanagement.resource.v1.request;

import javax.validation.constraints.Size;

import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@Data
public class RequestStreetByAreaFather {
	@ApiParam(value = "area father type", example = "Paises") 
	@Size(min=0, max=50)
	private String areaFatherType;
	
	@ApiParam(value = "area father name", example = "ARGENTINA") 
	@Size(min=0, max=50)
	private String areaFatherName;
}
