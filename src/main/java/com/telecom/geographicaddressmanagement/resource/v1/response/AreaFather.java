/**
 * 
 */
package com.telecom.geographicaddressmanagement.resource.v1.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@Data
@EqualsAndHashCode
@ApiModel( description="Padre de un area dentro de una jerarquia")
public class AreaFather {
	@ApiModelProperty(value = "Areas type", required = true, example = "Pais")
	@EqualsAndHashCode.Include
	private String type;
	
	@ApiModelProperty(value = "Area name.", required = true, example = "ARGENTINA")
	@EqualsAndHashCode.Include
	private String name;
	
	@ApiModelProperty(value = "nivel dentro de la jerarquia", required = true, example = "3")
	private Integer level;
	
	//@ApiModelProperty(value = "Identification", required = true, example = "ARGENTINA,BUENOS AIRES,TRES FEBRERO,CHURRUCA")
	@JsonIgnore
	private String identification;

}
