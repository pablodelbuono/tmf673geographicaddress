/**
 * 
 */
package com.telecom.geographicaddressmanagement.resource.v1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.Sets;
import com.telecom.geographicaddressmanagement.config.SwaggerConfig;
import com.telecom.geographicaddressmanagement.domain.repository.AreaFullTextSearchRepository;
import com.telecom.geographicaddressmanagement.infraestucture.exception.NotFoundException;
import com.telecom.geographicaddressmanagement.resource.v1.request.RequestAreaByIdentification;
import com.telecom.geographicaddressmanagement.resource.v1.request.RequestAreaByName;
import com.telecom.geographicaddressmanagement.resource.v1.request.RequestAreaByType;
import com.telecom.geographicaddressmanagement.resource.v1.request.RequestAreaFullTextSearch;
import com.telecom.geographicaddressmanagement.resource.v1.response.Area;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@RestController
@RequestMapping(value = "v1/areas", produces = MediaType.APPLICATION_JSON_UTF8_VALUE) // , consumes =
																						// MediaType.APPLICATION_JSON_VALUE)
@Api(tags = { SwaggerConfig.TAG_AREA })
public class AreaResource {
	private static final Logger logger = LoggerFactory.getLogger(AreaResource.class);
	
	@Value("${resource.cache.area:60}")
	private int HTTP_CACHE_MAX_AGE; 

	
	@Autowired
	private AreaFullTextSearchRepository fulltextSearchRepository;
	
	
	@GetMapping(params= {"name", "type"})
	@ApiOperation(value = "Get area by name and type")
	@ApiImplicitParam(name = "fields", paramType = "query",  value = "property filters, selects properties of an object using a subset of the Facebook Graph API filtering syntax", required = false, dataTypeClass = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved data from area."),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found. Not found areas.") })
	public ResponseEntity<List<Area>> searchByName(@Valid RequestAreaByName requestAreaByName) throws NotFoundException {
		int cant_total_result = 0;
		List<Area> areasPage = null;
		Set<Long> areaIdentifiers = null;		
		ResponseEntity<List<Area>> result;

		logger.info("Parametros de entrada: {}", requestAreaByName);
			
		areaIdentifiers = this.fulltextSearchRepository.getByTypeAndName(requestAreaByName.getType().toUpperCase(), requestAreaByName.getName().toUpperCase());	
		logger.debug("Nombre: {}",areaIdentifiers );
		
		cant_total_result = areaIdentifiers.size();					
		areasPage = areaIdentifiers.stream().map(fulltextSearchRepository::get).sorted(Comparator.comparing(Area::getName)).skip(requestAreaByName.getOffset()*requestAreaByName.getLimit()).limit(requestAreaByName.getLimit()).collect(Collectors.toList());
		logger.debug("Areas: {}", areasPage);
		
		if (CollectionUtils.isEmpty(areasPage)) {
			throw new NotFoundException();
		} else {
			if (cant_total_result == areasPage.size()) {
				result = ResponseEntity.ok().cacheControl(CacheControl.maxAge(HTTP_CACHE_MAX_AGE, TimeUnit.SECONDS)).header("X-TOTAL-COUNT", String.valueOf(cant_total_result)).body(areasPage);
			} else {
				result = ResponseEntity.status(HttpStatus.PARTIAL_CONTENT).cacheControl(CacheControl.maxAge(HTTP_CACHE_MAX_AGE, TimeUnit.SECONDS)).header("X-TOTAL-COUNT", String.valueOf(cant_total_result)).body(areasPage);
			}
		}		

		logger.info("Respuesta: {}", result);
		return result;			

	}
	
	@GetMapping(params= {"identification", "type"})
	@ApiOperation(value = "Get area by identification")
	@ApiImplicitParam(name = "fields", paramType = "query",  value = "property filters, selects properties of an object using a subset of the Facebook Graph API filtering syntax", required = false, dataTypeClass = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved data from area."),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found. Not found areas.") })
	public ResponseEntity<Area> searchByIdentification(@Valid RequestAreaByIdentification requestAreaByIdentification) throws NotFoundException {
		
		Area area = null;	
		Long idArea = null;
		ResponseEntity<Area> result;

		logger.info("Parametros de entrada: {}", requestAreaByIdentification);

		idArea = this.fulltextSearchRepository.getByTypeAndIdentification(requestAreaByIdentification.getType().toUpperCase(), String.join(",", requestAreaByIdentification.getIdentification().stream().map(String::toUpperCase).collect(Collectors.toList())));						
		logger.debug("Identification: {}",idArea );
		
		if (idArea != null) {
			area = this.fulltextSearchRepository.get(idArea);
			logger.debug("Area: {}", area);
		}
		
		if (area == null) {
			throw new NotFoundException();
		} else {
			result = ResponseEntity.ok().cacheControl(CacheControl.maxAge(HTTP_CACHE_MAX_AGE, TimeUnit.SECONDS)).header("X-TOTAL-COUNT", "1")
					.body(area);
		}
		
		logger.info("Respuesta: {}", result);
		return result;			

	}
	
	@GetMapping(params= {"type"})
	@ApiOperation(value = "Get area by type")
	@ApiImplicitParam(name = "fields", paramType = "query",  value = "property filters, selects properties of an object using a subset of the Facebook Graph API filtering syntax", required = false, dataTypeClass = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved data from area."),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found. Not found areas.") })
	public ResponseEntity<List<Area>> searchByType(@Valid RequestAreaByType requestAreaByType) throws NotFoundException {
		
		int cant_total_result = 0;
		List<Area> areasPage = null;
		Set<Long> areaIdentifiers = null;		
		ResponseEntity<List<Area>> result;

		logger.info("Parametros de entrada: {}", requestAreaByType);
			
		areaIdentifiers = this.fulltextSearchRepository.getByType(requestAreaByType.getType().toUpperCase());	
		logger.debug("Types: {}",areaIdentifiers );
	
		
		if (! CollectionUtils.isEmpty(areaIdentifiers)) {
			if ( (StringUtils.hasText(requestAreaByType.getFatherType())) && (! CollectionUtils.isEmpty(requestAreaByType.getFatherIdentification()))) {
				areaIdentifiers  = this.fulltextSearchRepository.getByFather(requestAreaByType.getFatherType().trim().toUpperCase(), String.join(",", requestAreaByType.getFatherIdentification().stream().map(String::toUpperCase).collect(Collectors.toList()))).stream().filter(areaIdentifiers::contains).collect(Collectors.toSet());
				logger.debug("Areas by father : {}", areaIdentifiers);						
			}
			
			cant_total_result = areaIdentifiers.size();					
			areasPage = areaIdentifiers.stream().map(fulltextSearchRepository::get).sorted(Comparator.comparing(Area::getName)).skip(requestAreaByType.getOffset()*requestAreaByType.getLimit()).limit(requestAreaByType.getLimit()).collect(Collectors.toList());				
		}		
		
		logger.debug("Areas: {}", areasPage);			
		if (CollectionUtils.isEmpty(areasPage)) {
			throw new NotFoundException();
		} else {
			if (cant_total_result == areasPage.size()) {
				result = ResponseEntity.ok().cacheControl(CacheControl.maxAge(HTTP_CACHE_MAX_AGE, TimeUnit.SECONDS)).header("X-TOTAL-COUNT", String.valueOf(cant_total_result)).body(areasPage);
			} else {
				result = ResponseEntity.status(HttpStatus.PARTIAL_CONTENT).cacheControl(CacheControl.maxAge(HTTP_CACHE_MAX_AGE, TimeUnit.SECONDS)).header("X-TOTAL-COUNT", String.valueOf(cant_total_result)).body(areasPage);
			}
		}		

		logger.info("Respuesta: {}", result);
		return result;			
	}
	
	@GetMapping(params= {"name", "type", "fullText=true"})	
	@ApiOperation(value = "Full text search by area name")
	@ApiImplicitParam(name = "fields", paramType = "query",  value = "property filters, selects properties of an object using a subset of the Facebook Graph API filtering syntax", required = false, dataTypeClass = String.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved data from area."),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found. Not found areas.") })
	public ResponseEntity<List<Area>> searchByName(@Valid RequestAreaFullTextSearch requestAreaByName) throws NotFoundException {
		ResponseEntity<List<Area>> result = null;
		List<Area> areasPage = null;
		Set<Long> areaIdentifiers = null;
		int cant_total_result = 0;
		
		logger.info("Parametros de entrada: {}", requestAreaByName);
		
		areaIdentifiers = Arrays.asList(StringUtils.tokenizeToStringArray(requestAreaByName.getName()," ")).stream().filter(token -> token.length() > 2).map(String::toUpperCase).map(fulltextSearchRepository::getByTextSearch)
		.reduce(Sets::intersection).get();			
		logger.debug("Areas by suggest: {}", areaIdentifiers);
		
		if (! CollectionUtils.isEmpty(areaIdentifiers)) {	
			areaIdentifiers = requestAreaByName.getType().stream().map(String::toUpperCase).map(fulltextSearchRepository::getByType).flatMap(Set::stream).filter(areaIdentifiers::contains).collect(Collectors.toSet());
			logger.debug("Areas by type : {}", areaIdentifiers);		
			
			if (! CollectionUtils.isEmpty(areaIdentifiers)) {
				if ( (StringUtils.hasText(requestAreaByName.getFatherType())) && (! CollectionUtils.isEmpty(requestAreaByName.getFatherIdentification()))) {
					areaIdentifiers  = this.fulltextSearchRepository.getByFather(requestAreaByName.getFatherType().trim().toUpperCase(), String.join(",", requestAreaByName.getFatherIdentification().stream().map(String::toUpperCase).collect(Collectors.toList()))).stream().filter(areaIdentifiers::contains).collect(Collectors.toSet());
					logger.debug("Areas by father : {}", areaIdentifiers);						
				}
			    
				logger.debug("id areas: {}", areaIdentifiers);
				
				cant_total_result = areaIdentifiers.size();					
				areasPage = areaIdentifiers.stream().map(fulltextSearchRepository::get).sorted(Comparator.comparing(Area::getName)).skip(requestAreaByName.getOffset()*requestAreaByName.getLimit()).limit(requestAreaByName.getLimit()).collect(Collectors.toList());
				
			}			
		}
	
		logger.debug("Areas: {}", areasPage);		
		if (CollectionUtils.isEmpty(areasPage)) {
			throw new NotFoundException();
		} else {
			if (cant_total_result == areasPage.size()) {
				result = ResponseEntity.ok().cacheControl(CacheControl.maxAge(HTTP_CACHE_MAX_AGE, TimeUnit.SECONDS)).header("X-TOTAL-COUNT", String.valueOf(cant_total_result)).body(areasPage);
			} else {
				result = ResponseEntity.status(HttpStatus.PARTIAL_CONTENT).cacheControl(CacheControl.maxAge(HTTP_CACHE_MAX_AGE, TimeUnit.SECONDS)).header("X-TOTAL-COUNT", String.valueOf(cant_total_result)).body(areasPage);
			}
		}		
		

		return result;

	}
}
