package com.telecom.geographicaddressmanagement.resource.v1.response;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.telecom.geographicaddressmanagement.config.SwaggerConfig;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel( description=SwaggerConfig.TAG_STREET_DESCRIPTION)
public class Street {
	@JsonIgnore
	private Long id;
	
	@ApiModelProperty(value = "Identification", required = true, example = "ARGENTINA,CAPITAL FEDERAL,CAPITAL FEDERAL,CAPITAL FEDERAL,SERRANO")
	private String identification;
	
	@ApiModelProperty(value = "Country that the address is in.", required = true, example = "ARGENTINA")
	private String country;
	
	@ApiModelProperty(value = "The State or Province that the address is in.", required = true, example = "CAPITAL FEDERAL")
	private String stateOrProvince;
	
	@ApiModelProperty(value = "City that the address is in.", required = true, example = "CAPITAL FEDERAL")
	private String city;
	
	@ApiModelProperty(value = "Locality that the address is in.", required = true, example = "CAPITAL FEDERAL")
	private String locality;
	
	@ApiModelProperty(value = "Street name.", required = true, example = "SERRANO")
	private String name;
		
	@ApiModelProperty(value = "Street type", required = true, example = "AVENIDA")
	private String type;
	
	@ApiModelProperty(value = "List of characteristics")
	private List<Characteristic> characteristics = new ArrayList<>();
	
}
