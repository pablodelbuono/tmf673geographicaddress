/**
 * 
 */
package com.telecom.geographicaddressmanagement.resource.v1.mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.telecom.geographicaddressmanagement.domain.model.DataArea;
import com.telecom.geographicaddressmanagement.resource.v1.response.Area;
import com.telecom.geographicaddressmanagement.resource.v1.response.AreaFather;
import com.telecom.geographicaddressmanagement.resource.v1.response.Characteristic;

/**
 * @author PABLODANIELDELBUONO
 *
 */
@Mapper(componentModel = "spring", nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT, nullValuePropertyMappingStrategy=NullValuePropertyMappingStrategy.SET_TO_DEFAULT)
public interface AreaMapper {
	
	public static final Logger logger = LoggerFactory.getLogger(AreaMapper.class);
	
	public List<Area> dataAreasToAreas(List<DataArea> dataArea);
	
	public Area toArea(DataArea dataAreaSuggest);
	
	default List<AreaFather> toFathers(String chrFathers) {
		List<AreaFather> result = null;
		AreaFather areaFather = null;
		String[] fathers;
		String[] fatherProperties;
		String identification = null;
		
		try {		
			if (StringUtils.hasText(chrFathers)) {
				result = new ArrayList<>();
				fathers = chrFathers.replaceFirst(";", "").split(";");
				for(String father : fathers) {
					fatherProperties = father.split("\\s*#\\s*");
					areaFather = new AreaFather();
					areaFather.setLevel(Integer.valueOf(fatherProperties[0]));
					areaFather.setType(fatherProperties[1]);
					areaFather.setName(fatherProperties[2]);
					areaFather.setIdentification(fatherProperties[2]);
					result.add(areaFather);
				}				
				
				//Arma el identification para los fathers
				for (AreaFather father : result.stream().sorted(Comparator.comparing(AreaFather::getLevel).reversed()).collect(Collectors.toList())) {					
					if (! StringUtils.hasText(identification)) {
						identification = father.getIdentification();
					} else {
						identification = String.join(",", identification, father.getIdentification());
						father.setIdentification(identification);
					}
				};
				
				
			} else {
				result = Collections.emptyList();
			}
		} catch (Exception ex) {
			logger.error("Error no esperado para el padre " + chrFathers, ex);
			result = Collections.emptyList();
		}
		
		return result;
	}
	
	default List<Characteristic>  toCharacteristic(String cCharacteristics) {
		List<Characteristic> result = null;
		Characteristic areaCharacteristic = null;
		String[] characteristics = null;
		String[] characteristicProperties = null;
		
		try {
			if (StringUtils.hasText(cCharacteristics)) {
				result = new ArrayList<>();
				characteristics = cCharacteristics.split(";");
				for(String characteristic : characteristics) {
					characteristicProperties = characteristic.split("\\s*#\\s*");				
					areaCharacteristic = new Characteristic();
					areaCharacteristic.setName(characteristicProperties[0]);
					
					if (characteristicProperties.length > 1) {
						areaCharacteristic.setValue(characteristicProperties[1]);
					}					
					result.add(areaCharacteristic);
					
				}
			} else {
				result = Collections.emptyList();
			}
		} catch (Exception ex) {
			logger.error("Error no esperado la propiedad " + cCharacteristics, ex);
			result = Collections.emptyList();
		}
		
		return result;
		
	}
//Coordenada X de punto medio#-62.0974521871761;Coordenada Y de punto medio#-32.6916031061651
//	public static void main(String[] args) {
//		String ejemplo = "Coordenada X de punto medio#-32.6916031061651;Coordenada Y de punto medio#";
//		List<Characteristic> result = toCharacteristic(ejemplo);
//		
//		for (Characteristic c : result) {
//			System.out.println(c.getName() + " - " + c.getValue());
//		}
//	}

}
