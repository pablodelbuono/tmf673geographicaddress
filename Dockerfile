
# Use an official jdk 8 runtime as a parent image
FROM openjdk:8-jdk-alpine

MAINTAINER delbuono@ar.ibm.com

# Copy the app contents into the container at /app.jar
ADD target/geographicaddressmanagement-0.0.1-SNAPSHOT.jar app.jar

ENV JAVA_OPTS="-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=2"

# Make port 80 available to the world outside this container
EXPOSE 8080

# To reduce Tomcat startup time we added a system property pointing to "/dev/urandom" as a source of entropy.
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -Duser.timezone=America/Argentina/Buenos_Aires -jar /app.jar